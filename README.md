# Hệ thống học tập trực tuyến có hỗ trợ video

## Đặc tả hệ thống
Hệ thống học tập trực tuyến có hỗ trợ video (VES) bao gồm các khóa học thuộc các chủ đề khác nhau (thông tin bao gồm: mã chủ đề, tên chủ đề). Một khóa học chỉ thuộc một chủ đề và một chủ đề có thể có nhiều khóa học. Thông tin một khóa học bao gồm mã khóa học, tên khóa học, học phí, số lượng tối đa học viên có thể tham gia và thông tin quy định khóa học là mở hoặc đóng.

Mỗi khóa học bao gồm nhiều bài học. Thông tin mỗi bài học bao gồm tên bài hoc, nội dung bài học, video của bài học, các file đính kèm theo. Video được lưu trữ bởi một bên thứ 3 để để đảm bảo các khóa học mở thì các học viên không đăng kí tham gia không thể xem được. Mỗi chủ đề học tập được hệ thống phân công cho một nhân viên quản lý.

Người dùng trong hệ thống bao gồm hai loại: quản trị viên hệ thống và học viên. Người quản trị hệ thống được sử dụng nhiều chức năng của hệ thống do quản trị viên tối cao phân động thông qua một bảng chức năng. Người quản trị hệ thống là nhân viên của đơn vị đang sử dụng VES, và có thể được phân công để quản lý mỗi chủ đề hoặc mỗi khóa học. Về học viên, người dùng có thể tạo tài khoản bằng việc sử dụng tài khoản Google hoặc tài khoản trực tiếp của hệ thống. Thông tin mỗi tài khoản bao gồm: tên đăng nhập (người dùng đặt đối với tài khoản hệ thống, mã GoogleID đối với tài khoản Google), mật khẩu (chỉ sử dụng với tài khoản hệ thống), thông tin cá nhân của học viên (họ và tên, ngày sinh, giới tính, số điện thoại, email).

Người dùng phải đăng nhập vào hệ thống phải xem được các khóa học (kể cả khóa học mở và đóng). Đối với các khóa học đóng, học viên phải đăng kí để có thể bắt đầu học. Quản trị viên sau khi xác nhận thanh toán, sẽ cấp phép cho học viên và gửi tin nhắn việc cấp phép cho học viên. 

Hệ thống còn hỗ trợ các bài trắc nghiệm để phục vụ cho việc đánh giá chất lượng khi học viên tham gia khóa học. Tuy nhiên, mỗi bài trắc nghiệm có thể thuộc một chủ đề, một khóa học hay không thuộc bất kì khóa học, chủ đề nào tùy thuộc vào mục đích sử dụng của người quản trị. Mỗi bài quiz bao gồm nhiều câu hỏi, mỗi câu hỏi có thể có nhiều đáp án trắc nghiệm (không giới hạn đáp án, tùy thuộc vào mục đích của người ra đề). Mỗi lần người dùng tham gia vào quiz, câu hỏi và đáp án được đảo thứ tự ngẫu nhiên hay không cũng phụ thuộc vào người ra đề. Nếu người ra đề đã lựa chọn đáp án, sau khi người tham gia làm bài sẽ biết điểm sau khi nộp bài hoặc hết thời gian làm bài.

## Công nghệ
Framework: Nest.js (tham khảo tại: https://nestjs.com/)
Database ORM: http://typeorm.io/
Tool ERD to DB: Visual Paradigm

## Hướng dẫn chạy project
1. Chạy lệnh: ```npm i``` để tải các node modules.
2. ```npm run start:dev``` để bắt đầu chạy project với port 8009.

## Hướng dẫn generate Database
1. Mở file ```.vpp``` bằng phần mềm Visual Paradigm 
2. Generate database (connect trực tiếp vào database)

## Phân chia công việc

STT | Công việc | Phân công | Hạn chót |
--- | --- | --- | --- |
1 | Đặc tả hệ thống | Trần Ngọc Bảo Duy | 20g00 30/10/2018 |
2 | Vẽ ERD hệ thống | Đào Nguyễn Anh | 20g00 01/10/2018 |
3 | Tìm hiểu công cụ ánh xạ ERD sang dạng table và sinh script tự động cho PostgreSQL | Nguyễn Duy Minh | 20g00 01/11/2018 |
4 | Ánh xạ trên công cụ và sinh code | Nguyễn Duy Minh | 20g00 02/11/2018 |
5 | Vẽ các lược đồ UML (P.1) | Đào Nguyễn Anh & Nguyễn Duy Minh | 20g00 06/11/2018 |
6 | Khởi tạo project và đặc tả tài liệu | Trần Ngọc Bảo Duy | 20g00 05/11/2018 |
