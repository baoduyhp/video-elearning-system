var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var uglify = require('gulp-uglify');

gulp.task('watch:scss', function() {
    gulp.watch('./src/Public/Styles/*/**/***.scss', function(event) {
        console.log(`${event.type}: ${event.path}`);
        var dirname = path.dirname(event.path);
        return gulp.src(event.path)
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(cssmin())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dirname));
    });
});

gulp.task('watch:js', function() {
    gulp.watch('./src/Public/Script/*/**/***.js', function(event) {
        if (event.path.toString().includes('min.js'))
            return;

        console.log(`${event.type}: ${event.path}`);
        var dirname = path.dirname(event.path);
        return gulp.src(event.path)
            .pipe(sourcemaps.init())
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dirname));
    });
});

gulp.task('default', ['watch:scss', 'watch:js']);
