import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import * as session from 'express-session';
import flash = require('connect-flash');
import * as favicon from 'serve-favicon';
import * as helmet from 'helmet';

//import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true
  }));
  app.use(helmet());
  // Test
  app.use(cookieParser());
  app.use(flash());
  app.use(favicon(__dirname + "/Public/favicon.ico"));
  app.useStaticAssets(__dirname + "/Public", {
    dotfiles: 'deny',
    index: false,
    redirect: false
  });
  app.setBaseViewsDir(__dirname + "/Views");
  app.setViewEngine('pug');
  
  
  await app.listen(8009);
}
bootstrap();
