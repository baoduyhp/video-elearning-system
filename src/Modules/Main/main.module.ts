import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HomeController } from './Home/home.controller';
import { User } from 'Models/user.entity';


@Module({
  imports: [ 
    TypeOrmModule.forRoot(), TypeOrmModule.forFeature([User])
  ],
  controllers: [ 
    HomeController
  ],
  providers: [ 
    
  ]
})
export class MainModule implements NestModule
{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .with('MainModule')
      .forRoutes("*")
      ;
  }
}
