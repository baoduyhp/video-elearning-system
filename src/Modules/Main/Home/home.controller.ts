import { Get, Controller, Render, Dependencies } from '@nestjs/common';

@Controller()
export class HomeController {
    @Get()
    @Render('Main/Home/Index')
    async root() {
        
    }
}