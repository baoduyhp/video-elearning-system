import slug = require('slug');
import * as numeral from 'numeral';

import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, UseGuards, Param } from '@nestjs/common';
import { Response, Request } from 'express';
import { RolesGuard } from 'Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';

import { CourseServiceImpl } from '../../../Services/Implementation/course.service.impl';
import { CourseService } from '../../../Services/course.service';
import { TopicServiceImpl } from 'Services/Implementation/topic.service.impl';
import { TopicService } from '../../../Services/topic.service';
import { Course } from '../../../Models/course.entity';
import { LessonServiceImpl } from '../../../Services/Implementation/lesson.service.impl';
import { LessonService } from '../../../Services/lesson.service';

@Controller("/quan-tri")
@Dependencies(CourseServiceImpl, TopicServiceImpl, LessonServiceImpl)
export class CourseController {
    constructor(private courseService: CourseService,
        private topicService: TopicService,
        private lessonService: LessonService) {}

    @Get("/khoa-hoc")
    @UseGuards(RolesGuard)
    @Roles("COURSE")
    async getCoursePage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            courseList: await this.courseService.findAll()
        };

        return res.render("Admin/Course/Index", viewBag);
    }

    @Get("/khoa-hoc/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_COURSE")
    async getAddingCoursePage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            topicList: (await this.topicService.findAll()).map(x => Object({
                text: `${x.id} - ${x.name}`,
                id: x.id
            }))
        };
        
        return res.render("Admin/Course/Create", viewBag);
    }

    @Get("/khoa-hoc/chinh-sua/:id")
    @UseGuards(RolesGuard)
    @Roles("EDIT_COURSE")
    async getEditCoursePage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        const viewBag = {
            course: await this.courseService.findOne(id),
            topicList: (await this.topicService.findAll()).map(x => Object({
                text: `${x.id} - ${x.name}`,
                id: x.id,
                selected: x.id === Number(id)
            }))
        };
        
        return res.render("Admin/Course/Edit", viewBag);
    }

    @Get("/khoa-hoc/chi-tiet/:id")
    @UseGuards(RolesGuard)
    @Roles("DETAILS_COURSE")
    async getDetailsPage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        const viewBag = {
            course: await this.courseService.findOne(id),
            lessonList: await this.lessonService.findAllByCourseId(id)
        };
        
        return res.render("Admin/Course/Details", viewBag);
    }

    @Post("/khoa-hoc/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_COURSE")
    async addCourse(@Body() course: Course, @Body('topicId') topicId: number, 
        @Req() req: Request, @Res() res: Response) {
            course.slug = slug(`${course.name} ${new Date().getTime()}`.toLowerCase());
            course.topic = await this.topicService.findOne(topicId);
            course.maxStudent = course.maxStudent ? numeral(course.maxStudent).value() : null;
            course.fee = course.fee ? numeral(course.fee).value() : null;
            course.published = false;

            await this.courseService.add(course);
            return res.redirect('/quan-tri/khoa-hoc');
    }

    @Post("/khoa-hoc/chinh-sua")
    @UseGuards(RolesGuard)
    @Roles("EDIT_COURSE")
    async editCourse(@Body() course: Course, @Body('topicId') topicId: number, 
        @Req() req: Request, @Res() res: Response) {
            course.topic = await this.topicService.findOne(topicId);
            course.maxStudent = !isNaN(course.maxStudent) ? course.maxStudent : null;
            course.fee = !isNaN(course.fee) ? course.fee : null;
            course.published = false;

            await this.courseService.update(course);
            return res.redirect('/quan-tri/khoa-hoc');
    }

    @Get("/khoa-hoc/doi-trang-thai/:id")
    @UseGuards(RolesGuard)
    @Roles("PUBLISH_COURSE")
    async publishCourse(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        await this.courseService.publish(id);
        
        return res.redirect('/quan-tri/khoa-hoc');
    }

    @Post("/khoa-hoc/xoa")
    @UseGuards(RolesGuard)
    @Roles("DELETE_COURSE")
    async deleteCourse(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
        await this.courseService.delete(id);
        
        return res.redirect('/quan-tri/khoa-hoc');
    }
}