import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus } from '@nestjs/common';
import { Response, Request } from 'express';
import { AuthenticationServiceImpl } from 'Services/Implementation/authentication.service.impl';
import { AuthenticationService } from 'Services/authentication.service';
import { AuthenticationDTO, AuthenticationMessageDTO } from 'Helpers/DTO/authentication.dto';
import { AuthenticationStatus } from 'Helpers/Constant/authentication.constant';


@Controller("/quan-tri")
@Dependencies(AuthenticationServiceImpl)
export class AuthenticationController {
    constructor(private authenticationService: AuthenticationService) {}

    @Get('/dang-nhap')
    loadSignInPage(@Req() req: Request, @Res() res: Response) {
        // TO-DO: Fix logic after fist UAT
        var token = req.cookies['ves'];
        if (token)
            return res.redirect('/quan-tri/trang-ca-nhan');

        return res.render('Admin/Authentication/Index', {
            authMessage: req.flash('authMessage')
        });
    }

    @Post('/dang-nhap')
    async login(@Body() authenticationDTO: AuthenticationDTO, @Req() req: Request, @Res() res: Response) {
        try {
            var signInMessage: AuthenticationMessageDTO = await this.authenticationService.sign(authenticationDTO);
        }
        catch (ex) {
            req.flash('authMessage', 'Đã xảy ra lỗi trong quá trình đăng nhập. Vui lòng thử lại');
            return res.redirect('/quan-tri/dang-nhap');
        }
                
        if (signInMessage.status === AuthenticationStatus.OK) {
            res.cookie('ves', signInMessage.token);
            return res.redirect('/quan-tri/trang-ca-nhan');
        }
        
        req.flash('authMessage', 'Đăng nhập thất bại');
        return res.redirect('/quan-tri/dang-nhap');
    }

    @Get('/dang-xuat')
    logout(@Req() req: Request, @Res() res: Response) {
        res.clearCookie('ves');
        res.redirect('/quan-tri/dang-nhap');        
    }
}