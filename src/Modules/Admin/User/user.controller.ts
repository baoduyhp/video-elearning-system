import * as crypto from 'crypto';
import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { Response, Request } from 'express';
import { User } from 'Models/user.entity';
import { UserServiceImpl } from 'Services/Implementation/user.service.impl';
import { UserService } from 'Services/user.service';
import { UserStatus } from 'Helpers/Constant/user.constant';
import { RoleServiceImpl } from '../../../Services/Implementation/role.service.impl';
import { RoleService } from '../../../Services/role.service';
import { RolesGuard } from '../../../Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';


@Controller("/quan-tri")
@Dependencies(UserServiceImpl, RoleServiceImpl)
export class UserController {
    constructor(private userService: UserService,
        private roleService: RoleService) {}

    @Get("/tai-khoan")
    @UseGuards(RolesGuard)
    @Roles("USER")
    async getHomePage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            userList: (await this.userService.findAll()).filter(u => u.isAdmin),
            roleList: (await this.roleService.findAll()).map(f => Object({
                text: `${f.id} - ${f.title}`,
                id: f.id
            }))
        }

        return res.render("Admin/User/Index", viewBag);
    }

    @Post("/tai-khoan/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_USER")
    async addNewUser(@Body() user: User, @Body('roleId') roleId: number, @Req() req: Request, @Res() res: Response) {
        user.password = crypto.createHmac('sha256', user.password).digest('hex');
        user.status = UserStatus.ACTIVE;
        user.role = await this.roleService.findOne(roleId);

        await this.userService.add(user);
        return res.redirect('/quan-tri/tai-khoan');
    }

    @Post("/tai-khoan/chinh-sua")
    @UseGuards(RolesGuard)
    @Roles("EDIT_USER")
    async editUser(@Body() user: User, @Body('roleId') roleId: number, @Req() req: Request, @Res() res: Response) {
        var userToEdit: User = await this.userService.findOne(user.username);
        userToEdit.displayName = user.displayName.trim();
        userToEdit.role = await this.roleService.findOne(roleId);
        if (user.password)
            userToEdit.password = crypto.createHmac('sha256', user.password).digest('hex');

        await this.userService.save(userToEdit);
        return res.redirect('/quan-tri/tai-khoan');
    }

    @Get("/tai-khoan/doi-trang-thai/:username")
    @UseGuards(RolesGuard)
    @Roles("PUBLISH_USER")
    async publishUser(@Param('username') username: string, @Req() req: Request, @Res() res: Response) {
        let userToPublish: User = await this.userService.findOne(username);
        if (userToPublish) {
            userToPublish.status = (userToPublish.status === UserStatus.ACTIVE) ? UserStatus.INACTIVE : UserStatus.ACTIVE;
            await this.userService.save(userToPublish);
        }
        return res.redirect('/quan-tri/tai-khoan');
    }

    // @Post("/tai-khoan/chinh-sua")
    // async editNewUser(@Body() user: User, @Req() req: Request, @Res() res: Response) {
    //     user.name = user.name.trim();
    //     await this.userService.update(user);
    //     return res.redirect('/quan-tri/tai-khoan');
    // }

    // @Post("/tai-khoan/xoa")
    // async deleteNewUser(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
    //     const existedUser: User = await this.userService.findOne(id);

    //     if (existedUser && existedUser.courses.length > 0) req.flash('UserMessage', UserMessage.EXISTED_REFERENCE);
    //     else await this.userService.delete(id);
    //     return res.redirect('/quan-tri/tai-khoan');
    // }

   
}