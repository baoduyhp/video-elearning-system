import * as fs from 'fs';
import * as path from 'path';

import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, UseGuards, Param, UseInterceptors, FileInterceptor, UploadedFile } from '@nestjs/common';
import { Response, Request } from 'express';
import { RolesGuard } from 'Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';

import { FileServiceImpl } from 'Services/Implementation/file.service.impl';
import { FileService } from 'Services/file.service';
import { User } from 'Models/user.entity';
import { PUBLIC_PATH } from 'Helpers/Constant/path.constant';
import { File } from 'Models/file.entity';
import { UserServiceImpl } from 'Services/Implementation/user.service.impl';
import { UserService } from 'Services/user.service';

@Controller("/quan-tri")
@Dependencies(FileServiceImpl, UserServiceImpl)
export class FileController {
    constructor(private fileService: FileService,
        private userService: UserService) {}

    @Get("/file")
    @UseGuards(RolesGuard)
    @Roles("FILE")
    async getFilePage(@Req() req: Request, @Res() res: Response) {
        const user: User = req.session.user;

        const viewBag = {
            fileList: (await this.fileService.findAll()).filter(f => f.createdUser.username === user.username)
        };

        return res.render("Admin/File/Index", viewBag);
    }

    @Post('/file/them-moi')
    @UseGuards(RolesGuard)
    @Roles("FILE")
    @UseInterceptors(FileInterceptor('file', { dest: PUBLIC_PATH + '/Assets/Files' }))
    async addFile(@UploadedFile() file, @Body() fileInfo: File, @Res() res: Response, @Req() req: Request) {
        if (file) {
            const extension: string = path.extname(file.originalname);
            file.filename += extension;

            const newPath: string = `${file.destination}/${file.filename}`;
            fs.renameSync(file.path, newPath);
            if (fileInfo.displayName)
                fileInfo.displayName = fileInfo.displayName.toString();
            else fileInfo.displayName = file.originalname;

            fileInfo.path = newPath;
            fileInfo.filetype = extension;
            fileInfo.filename = file.originalname;
            fileInfo.size = Math.round(file.size / 1000.0);

            fileInfo.createdUser = await this.userService.findOne(req.session.user.username);

            await this.fileService.add(fileInfo);
        }
        
        return res.redirect('/quan-tri/file');
    }

    // @Get("/khoa-hoc/them-moi")
    // @UseGuards(RolesGuard)
    // @Roles("ADD_COURSE")
    // async getAddingCoursePage(@Req() req: Request, @Res() res: Response) {
    //     const viewBag = {
    //         topicList: (await this.topicService.findAll()).map(x => Object({
    //             text: `${x.id} - ${x.name}`,
    //             id: x.id
    //         }))
    //     };
        
    //     return res.render("Admin/Course/Create", viewBag);
    // }

    // @Get("/khoa-hoc/chinh-sua/:id")
    // @UseGuards(RolesGuard)
    // @Roles("EDIT_COURSE")
    // async getEditCoursePage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
    //     const viewBag = {
    //         course: await this.courseService.findOne(id),
    //         topicList: (await this.topicService.findAll()).map(x => Object({
    //             text: `${x.id} - ${x.name}`,
    //             id: x.id,
    //             selected: x.id === Number(id)
    //         }))
    //     };
        
    //     return res.render("Admin/Course/Edit", viewBag);
    // }

    // @Get("/khoa-hoc/chi-tiet/:id")
    // @UseGuards(RolesGuard)
    // @Roles("DETAILS_COURSE")
    // async getDetailsPage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
    //     const viewBag = {
    //         course: await this.courseService.findOne(id),
    //         lessonList: await this.lessonService.findAllByCourseId(id)
    //     };
        
    //     return res.render("Admin/Course/Details", viewBag);
    // }

    // @Post("/khoa-hoc/them-moi")
    // @UseGuards(RolesGuard)
    // @Roles("ADD_COURSE")
    // async addCourse(@Body() course: Course, @Body('topicId') topicId: number, 
    //     @Req() req: Request, @Res() res: Response) {
    //         course.slug = slug(`${course.name} ${new Date().getTime()}`.toLowerCase());
    //         course.topic = await this.topicService.findOne(topicId);
    //         course.maxStudent = course.maxStudent ? numeral(course.maxStudent).value() : null;
    //         course.fee = course.fee ? numeral(course.fee).value() : null;
    //         course.published = false;

    //         await this.courseService.add(course);
    //         return res.redirect('/quan-tri/khoa-hoc');
    // }

    @Post("/file/chinh-sua")
    @UseGuards(RolesGuard)
    @Roles("FILE")
    async editCourse(@Body() file: File, @Body('topicId') topicId: number, 
        @Req() req: Request, @Res() res: Response) {
            var fileToEdit: File = await this.fileService.findOne(file.id);
            fileToEdit.displayName = file.displayName.trim();

            await this.fileService.update(fileToEdit);
            return res.redirect('/quan-tri/file');
    }

    // @Get("/khoa-hoc/doi-trang-thai/:id")
    // @UseGuards(RolesGuard)
    // @Roles("PUBLISH_COURSE")
    // async publishCourse(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
    //     await this.courseService.publish(id);
        
    //     return res.redirect('/quan-tri/khoa-hoc');
    // }

    @Post("/file/xoa")
    @UseGuards(RolesGuard)
    @Roles("FILE")
    async deleteCourse(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
        await this.fileService.delete(id);
        
        return res.redirect('/quan-tri/file');
    }
}