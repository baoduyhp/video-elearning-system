import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, UseGuards } from '@nestjs/common';
import { Response, Request } from 'express';
import { RolesGuard } from 'Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';


@Controller("/quan-tri")
@Dependencies()
export class HomeController {
    constructor() {}

    @Get("/trang-ca-nhan")
    @UseGuards(RolesGuard)
    @Roles("ALL")
    getHomePage(@Req() req: Request, @Res() res: Response) {
        return res.render("Admin/Home/Index");
    }

   
}