import slug = require('slug');
import * as numeral from 'numeral';

import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, UseGuards, Param } from '@nestjs/common';
import { Response, Request } from 'express';
import { RolesGuard } from 'Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';

import { CourseServiceImpl } from '../../../Services/Implementation/course.service.impl';
import { CourseService } from '../../../Services/course.service';
import { Course } from '../../../Models/course.entity';
import { Lesson } from '../../../Models/lesson.entity';
import { LessonService } from '../../../Services/lesson.service';
import { LessonServiceImpl } from '../../../Services/Implementation/lesson.service.impl';
import { FileServiceImpl } from 'Services/Implementation/file.service.impl';
import { FileService } from 'Services/file.service';

@Controller("/quan-tri")
@Dependencies(CourseServiceImpl, LessonServiceImpl, FileServiceImpl)
export class LessonController {
    constructor(private courseService: CourseService,
        private lessonService: LessonService, 
        private fileService: FileService) {}

    @Get("/bai-hoc/:id/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_LESSON")
    async getAddingPage(@Param('id') courseId: number, @Req() req: Request, @Res() res: Response) {
        const viewBag = {
            course: await this.courseService.findOne(courseId),
            fileList: (await this.fileService.findAll()).filter(f => f.createdUser.username === req.session.user.username && !f.hidden).map(f => Object({
                id: f.id,
                text: `${f.displayName} - ${f.filename}`
            }))
        };
        
        return res.render("Admin/Lesson/Create", viewBag);
    }

    @Post("/bai-hoc/:id/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_LESSON")
    async addLesson(@Param('id') courseId: number, @Body() lesson: Lesson, @Body('filesId') filesId: number[], @Req() req: Request, @Res() res: Response) {
        if (filesId)
        {
            if (Array.isArray(filesId)) {
                lesson.files = [];
                for (const id of filesId) {
                    lesson.files.push(await this.fileService.findOne(id));
                }
            }
            else lesson.files = [ await this.fileService.findOne(filesId) ];
        }
        else lesson.files = [];
        lesson.name = lesson.name.trim();
        lesson.slug = slug(`${lesson.name.toLowerCase()} ${new Date().getTime()}`);
        lesson.course = await this.courseService.findOne(courseId);
        lesson.order = (await this.lessonService.findAllByCourseId(courseId)).length;

        await this.lessonService.add(lesson);
        
        return res.redirect("/quan-tri/khoa-hoc/chi-tiet/" + courseId);
    }

    @Get("/bai-hoc/chinh-sua/:id")
    @UseGuards(RolesGuard)
    @Roles("EDIT_LESSON")
    async getEditPage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        const lesson: Lesson = await this.lessonService.findOne(id);
        const viewBag = {
            lesson: lesson,
            fileList: (await this.fileService.findAll()).filter(f => f.createdUser.username === req.session.user.username && !f.hidden).map(f => Object({
                id: f.id,
                text: `${f.displayName} - ${f.filename}`,
                selected: lesson.files.findIndex(file => file.id === f.id) >= 0
            }))
        }

        return res.render("Admin/Lesson/Edit", viewBag);
    }

    @Post("/bai-hoc/chinh-sua/:id")
    @UseGuards(RolesGuard)
    @Roles("EDIT_LESSON")
    async editLesson(@Param('id') id: number, @Body() lesson: Lesson, @Body('filesId') filesId: number[], @Req() req: Request, @Res() res: Response) {
        const lessonToSave: Lesson = await this.lessonService.findOne(id);
        if (lessonToSave)
        {
            if (filesId)
            {
                if (Array.isArray(filesId)) {
                    lessonToSave.files = [];
                    for (const id of filesId) {
                        lessonToSave.files.push(await this.fileService.findOne(id));
                    }
                }
                else lessonToSave.files = [ await this.fileService.findOne(filesId) ];
            }
            else lessonToSave.files = [];
            
            lessonToSave.name = lesson.name.trim();
            lessonToSave.content = lesson.content;
            lessonToSave.videoUrl = lesson.videoUrl;
            lessonToSave.published = false;

            await this.lessonService.update(lessonToSave);
        }



        return res.redirect("/quan-tri/khoa-hoc/chi-tiet/" + lessonToSave.course.id);
    }

    @Get("/bai-hoc/doi-trang-thai/:id")
    @UseGuards(RolesGuard)
    @Roles("PUBLISH_LESSON")
    async publishLesson(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        await this.lessonService.publish(id);
        
        return res.redirect('/quan-tri/khoa-hoc/chi-tiet/' + (await this.lessonService.findOne(id)).course.id);
    }

    @Post("/bai-hoc/xoa")
    @UseGuards(RolesGuard)
    @Roles("DELETE_LESSON")
    async deleteCourse(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
        await this.lessonService.delete(id);
        
        return res.redirect('/quan-tri/khoa-hoc/chi-tiet/' + (await this.lessonService.findOne(id)).course.id);
    }
 
}