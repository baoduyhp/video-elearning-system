import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from 'Models/user.entity';
import { Topic } from 'Models/topic.entity';
import { Feature } from 'Models/feature.entity';
import { Role } from 'Models/role.entity';
import { Course } from 'Models/course.entity';
import { File } from 'Models/file.entity';
import { Lesson } from '../../Models/lesson.entity';

import { UserServiceImpl } from 'Services/Implementation/user.service.impl';
import { AuthenticationServiceImpl } from 'Services/Implementation/authentication.service.impl';
import { TopicServiceImpl } from 'Services/Implementation/topic.service.impl';
import { FeatureServiceImpl } from 'Services/Implementation/feature.service.impl';
import { RoleServiceImpl } from 'Services/Implementation/role.service.impl';
import { CourseServiceImpl } from '../../Services/Implementation/course.service.impl';
import { LessonServiceImpl } from '../../Services/Implementation/lesson.service.impl';
import { FileServiceImpl } from 'Services/Implementation/file.service.impl';

import { InitializationController } from './Initialization/initialization.controller';
import { HomeController } from './Home/home.controller';
import { AuthenticationController } from './Authentication/authentication.controller';
import { TopicController } from './Topic/topic.controller';
import { CourseController } from './Course/course.controller';
import { LessonController } from './Lesson/lesson.controller';
import { RoleController } from './Role/role.controller';
import { UserController } from './User/user.controller';
import { FileController } from './File/file.controller';

import { LocalMiddleware } from 'Helpers/Middleware/locals.middleware';
import { MenuMiddleware } from 'Helpers/Middleware/admin-menu.middleware';
import { RolesGuard } from 'Helpers/Guards/roles.guard';

@Module({
  imports: [ 
    TypeOrmModule.forRoot(), TypeOrmModule.forFeature([User, Topic, Feature, Role, Course, Lesson, File])
  ],
  controllers: [ 
    InitializationController, AuthenticationController, HomeController, TopicController, CourseController,
    LessonController, RoleController, UserController, FileController
  ],
  providers: [ 
    UserServiceImpl, AuthenticationServiceImpl, TopicServiceImpl, FeatureServiceImpl, RoleServiceImpl,
    CourseServiceImpl, LessonServiceImpl, FileServiceImpl,
    RolesGuard
  ]
})
export class AdminModule implements NestModule
{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LocalMiddleware)
      .with('AdminModule')
      .forRoutes("/quan-tri/*")
      ;

    consumer
      .apply(MenuMiddleware)
      .with('AdminModule')
      .forRoutes("/quan-tri/*")
      ;
  }
}
