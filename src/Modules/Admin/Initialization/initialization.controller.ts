import * as fs from 'fs';
import * as crypto from 'crypto';
import { Get, Controller, Dependencies, Req, Res, Query, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import * as Excel from 'exceljs';

import { UserServiceImpl } from 'Services/Implementation/user.service.impl';
import { UserService } from 'Services/user.service';
import { User } from 'Models/user.entity';

import { Feature } from 'Models/feature.entity';
import { FeatureService } from 'Services/feature.service';
import { FeatureServiceImpl } from 'Services/Implementation/feature.service.impl';

import { Role } from 'Models/role.entity';
import { RoleServiceImpl } from 'Services/Implementation/role.service.impl';
import { RoleService } from 'Services/role.service';
import { RoleTitle } from 'Helpers/Constant/role.constant';
import { UserStatus } from 'Helpers/Constant/user.constant';


@Controller("/quan-tri")
@Dependencies(UserServiceImpl, FeatureServiceImpl, RoleServiceImpl)
export class InitializationController {

    constructor(private userService: UserService, 
    private featureService: FeatureService,
    private roleService: RoleService) { }

    @Get("/khoi-tao-he-thong")
    async initialize(@Query('force') force: boolean, @Res() res: Response) {
        if (force) {
            var adminRole: Role = await this.roleService.findOneByTitle("Admin");
            if (!adminRole) {
                adminRole = new Role();
                adminRole.title = RoleTitle.ADMIN;
            }
            
            adminRole.features = await this.featureService.findAll();
            await this.roleService.addOrUpdate(adminRole);

            var adminUser: User = await this.userService.findOne("admin");
            if (!adminUser) {
                adminUser = new User();
                adminUser.username = "admin";
                adminUser.password = crypto.createHmac('sha256', process.env.PASSWORD).digest('hex');
                adminUser.displayName = "Administrator";
                adminUser.gender = true;
                adminUser.status = UserStatus.ACTIVE;
                adminUser.role = adminRole;

                await this.userService.add(adminUser);
            }

            var defaultRole: Role = await this.roleService.findOneByTitle("Mặc định (không chức năng)");
            if (!defaultRole) {
                defaultRole = new Role();
                defaultRole.title = RoleTitle.DEFAULT;
            }
            
            await this.roleService.addOrUpdate(defaultRole);

            return res.redirect("/quan-tri/dang-nhap");
        }
        else return res.status(HttpStatus.NOT_FOUND).send();
    }

    @Get("/khoi-tao-chuc-nang")
    async importFeatures(@Query('force') force: boolean, @Res() res: Response) {
        if (!force) return res.redirect('/quan-tri/dang-nhap');
        let workbook: Excel.Workbook = new Excel.Workbook();
        await workbook.xlsx.readFile(__dirname + '/Resource/features.xlsx');

        for (let worksheet of workbook.worksheets) {
            for (let i = 2; i <= worksheet.rowCount; i++) {
                let feature = new Feature();
                feature.code = worksheet.getRow(i).getCell(1).toString().trim();
                feature.name = worksheet.getRow(i).getCell(2).toString().trim();
                feature.onMenu = worksheet.getRow(i).getCell(3).toString().trim() === 'true';
                feature.menuTitle = worksheet.getRow(i).getCell(4).toString().trim();
                feature.link = worksheet.getRow(i).getCell(5).toString().trim();
                feature.icon = worksheet.getRow(i).getCell(6).toString().trim();
                feature.alwaysVisible = worksheet.getRow(i).getCell(7).toString().trim() === 'true';
                feature.order = Number(worksheet.getRow(i).getCell(8).toString().trim());
                let parentFeatureCode = worksheet.getRow(i).getCell(9).toString().trim();
                feature.parentFeature = parentFeatureCode === 'NULL' ? null : await this.featureService.findOne(parentFeatureCode);
                
                await this.featureService.addOrUpdate(feature);
            }
        }

        return res.redirect("/quan-tri/dang-nhap");
    }

}