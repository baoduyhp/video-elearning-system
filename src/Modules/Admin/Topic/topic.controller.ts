import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, Param } from '@nestjs/common';
import { Response, Request } from 'express';
import { Topic } from 'Models/topic.entity';
import { TopicServiceImpl } from 'Services/Implementation/topic.service.impl';
import { TopicService } from 'Services/topic.service';
import { TopicMessage } from 'Helpers/Constant/topic.constant';


@Controller("/quan-tri")
@Dependencies(TopicServiceImpl)
export class TopicController {
    constructor(private topicService: TopicService) {}

    @Get("/chu-de")
    async getHomePage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            topicList: await this.topicService.findAll(),
            topicMessage: req.flash('TopicMessage')
        }

        return res.render("Admin/Topic/Index", viewBag);
    }

    @Post("/chu-de/them-moi")
    async addNewTopic(@Body() topic: Topic, @Req() req: Request, @Res() res: Response) {
        topic.name = topic.name.trim();
        await this.topicService.add(topic);

        req.flash('TopicMessage', TopicMessage.ADD_TOPIC_SUCCESSFULLY);
        return res.redirect('/quan-tri/chu-de');
    }

    @Post("/chu-de/chinh-sua")
    async editNewTopic(@Body() topic: Topic, @Req() req: Request, @Res() res: Response) {
        topic.name = topic.name.trim();
        await this.topicService.update(topic);
        return res.redirect('/quan-tri/chu-de');
    }

    @Post("/chu-de/xoa")
    async deleteNewTopic(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
        const existedTopic: Topic = await this.topicService.findOne(id);

        if (existedTopic && existedTopic.courses.length > 0) req.flash('TopicMessage', TopicMessage.EXISTED_REFERENCE);
        else await this.topicService.delete(id);
        return res.redirect('/quan-tri/chu-de');
    }

   
}