import slug = require('slug');
import * as numeral from 'numeral';

import { Get, Controller, Render, Dependencies, Post, Body, Res, Req, HttpStatus, UseGuards, Param } from '@nestjs/common';
import { Response, Request } from 'express';
import { RolesGuard } from 'Helpers/Guards/roles.guard';
import { Roles } from 'Helpers/Decorators/roles.decorator';

import { RoleServiceImpl } from '../../../Services/Implementation/role.service.impl';
import { RoleService } from '../../../Services/role.service';
import { Role } from '../../../Models/role.entity';
import { RoleTitle } from '../../../Helpers/Constant/role.constant';
import { FeatureServiceImpl } from '../../../Services/Implementation/feature.service.impl';
import { FeatureService } from '../../../Services/feature.service';
import { UserServiceImpl } from '../../../Services/Implementation/user.service.impl';
import { UserService } from '../../../Services/user.service';

@Controller("/quan-tri")
@Dependencies(RoleServiceImpl, FeatureServiceImpl, UserServiceImpl)
export class RoleController {
    constructor(private roleService: RoleService,
        private featureService: FeatureService,
        private userService: UserService) {}

    @Get("/vai-tro")
    @UseGuards(RolesGuard)
    @Roles("ROLE")
    async getHomePage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            roleList: (await this.roleService.findAll()).map(role => Object({
                id: role.id,
                title: role.title,
                features: role.features,
                disabled: role.title === RoleTitle.ADMIN || role.title === RoleTitle.DEFAULT,
                createdAt: role.createdAt,
                updatedAt: role.updatedAt
            }))
        }

        return res.render("Admin/Role/Index", viewBag);
    }

    @Get("/vai-tro/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_ROLE")
    async getAddingPage(@Req() req: Request, @Res() res: Response) {
        const viewBag = {
            featureList: await this.featureService.findAll()
        }

        return res.render("Admin/Role/Create", viewBag);
    }

    @Post("/vai-tro/them-moi")
    @UseGuards(RolesGuard)
    @Roles("ADD_ROLE")
    async addNewRole(@Body() role: Role, @Body('featureCode') featureCodeList: string[], @Req() req: Request, @Res() res: Response) {
        if (featureCodeList)
        {
            if (Array.isArray(featureCodeList)) {
                role.features = [];
                for (const featureCode of featureCodeList) {
                    role.features.push(await this.featureService.findOne(featureCode));
                }
            }
            else role.features = [ await this.featureService.findOne(featureCodeList) ];
        }
        else role.features = [];

        await this.roleService.addOrUpdate(role);
        // role.title = role.title.trim();
        // await this.roleService.addOrUpdate(role);

        // req.flash('RoleMessage', RoleMessage.ADD_TOPIC_SUCCESSFULLY);
        return res.redirect('/quan-tri/vai-tro');
    }

    @Get("/vai-tro/chinh-sua/:id")
    @UseGuards(RolesGuard)
    @Roles("EDIT_ROLE")
    async getEditPage(@Param('id') id: number, @Req() req: Request, @Res() res: Response) {
        const viewBag = {
            featureList: await this.featureService.findAll(),
            role: await this.roleService.findOne(id)
        }
        return res.render("Admin/Role/Edit", viewBag);
    }

    @Post("/vai-tro/chinh-sua")
    @UseGuards(RolesGuard)
    @Roles("EDIT_ROLE")
    async editNewRole(@Body('featureCode') featureCodeList: string[], @Body() role: Role, @Req() req: Request, @Res() res: Response) {
        var roleToSave: Role = await this.roleService.findOne(role.id);
        roleToSave.title = role.title.trim();
        if (featureCodeList)
        {
            if (Array.isArray(featureCodeList)) {
                roleToSave.features = [];
                for (const featureCode of featureCodeList) {
                    roleToSave.features.push(await this.featureService.findOne(featureCode));
                }
            }
            else roleToSave.features = [ await this.featureService.findOne(featureCodeList) ];
        }
        else roleToSave.features = [];

        await this.roleService.addOrUpdate(roleToSave);
        return res.redirect('/quan-tri/vai-tro');
    }

    @Post("/vai-tro/xoa")
    @UseGuards(RolesGuard)
    @Roles("DELETE_ROLE")
    async deleteNewRole(@Body('id') id: number, @Req() req: Request, @Res() res: Response) {
        var roleToDelete: Role = await this.roleService.findOne(id);
        if (roleToDelete) {
            if (roleToDelete.users.length > 0) {
                var defaultRole: Role = await this.roleService.findOneByTitle(RoleTitle.DEFAULT);
                for (var user of roleToDelete.users) {
                    user.role = defaultRole;
                    await this.userService.save(user);
                }
            }

            await this.roleService.delete(id);
        }
        
        return res.redirect('/quan-tri/vai-tro');
    }
 
}