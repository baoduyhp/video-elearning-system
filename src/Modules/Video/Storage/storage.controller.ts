import { Get, Controller, Render, Dependencies } from '@nestjs/common';

@Controller("/video")
export class StorageController {
    @Get('/upload')
    @Render('Video/Storage/upload')
    async root() {
        
    }
}