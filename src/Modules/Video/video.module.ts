import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StorageController } from './Storage/storage.controller';
import { User } from 'Models/user.entity';


@Module({
  imports: [ 
    TypeOrmModule.forRoot(), TypeOrmModule.forFeature([User])
  ],
  controllers: [ 
    StorageController
  ],
  providers: [ 
    
  ]
})
export class VideoModule implements NestModule
{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .with('VideoModule')
      .forRoutes("*")
      ;
  }
}
