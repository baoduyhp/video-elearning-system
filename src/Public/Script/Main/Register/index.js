$(document).ready(function(){

    $('input[name=birthday]').focusout(function() {
        const value = $(this).val();
        if (!value) {
            $(this).attr('type', 'text');
        }
    });

    $.datepicker.setDefaults($.datepicker.regional['vi']);

    $("#inputDate").datepicker({
        dateFormat: "dd/mm/yy",
        showOn: "both",
        buttonImageOnly: true,
        buttonImage: "/Assets/Images/Icons/small-calendar.png",
        changeMonth: true,
        changeYear: true,
        yearRange: "-80:+0"
    });
    //-> lấy ngày ra =  var currentDate = $( ".selector" ).datepicker( "getDate" );
    var instructionSlides = JSON.parse($('input[name=instructionSlides]').val());
    var instructionImages = instructionSlides.map(slide => slide.image && slide.image.length > 0 ? slide.image : "/Assets/Images/Elements/no-img.png");

    $(".changeIns").click(function(){
        var thisIns = $(this);
        $('.ins-steps').find('*').removeClass("active");
        thisIns.find(".dots").addClass("active");
        thisIns.find(".step-text").addClass("active");

        var number = Number(thisIns.find('.dots').text());
        $('.ip-step img').attr("src", instructionImages[number - 1]);
    });

})