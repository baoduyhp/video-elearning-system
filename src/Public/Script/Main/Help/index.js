$(document).ready(function(){
    
    $('.tab-collapse').click(function(){
        $(this).next().slideToggle();
        $(this).find('.tab-icon').toggleClass("icon-arrow-down icon-arrow-up")
    })

    var branches = JSON.parse($('input[name=branches]').val());
    var centerBranch = branches.find(branch => branch.center);

    if (!centerBranch) {
        centerBranch = {
            longitude: 106.688139,
            latitude: 10.776743
        }
    }

    var map = new GMaps({
        div: '#map',
        lat: centerBranch.latitude,
        lng: centerBranch.longitude
    });

    for (const branch of branches) {
        map.addMarker({
            lat: branch.latitude,
            lng: branch.longitude,
            title: branch.name,
            icon: branch.classification === 'BRANCH' ? '/Assets/Images/Icons/livebank.png' : '/Assets/Images/Icons/branch.png',
            infoWindow: {
                content: `<strong>${branch.name}</strong><p>${branch.address}</p>`
            }
        });
    }
    $('.tab-control a').click(function(){
        
        if ($(this).hasClass("inactive")) {
            
            $(this).toggleClass("active inactive");
            $(this).siblings().toggleClass("active inactive");
        }

        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                window.location.hash = hash;
            });
        }
        
    });


});
