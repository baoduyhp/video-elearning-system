window.onscroll = function() {
    // console.log(window.pageYOffset); 
    scrollFix(); 
    changeScrollClass();
};


var peanut = 0;
var phone = 0;
var bgHeight = 0;
var cal = 0;
var h = 0;

var x = window.matchMedia("(min-width: 1600px")
setbgHeight(x)
x.addListener(setbgHeight);

$( window ).resize(setbgHeight);

$(document).ready(function(){
    peanut = $(".peanut-list").offset().top;
    phone = $("#iphone").offset().top;
    // console.log(cal);
    $(".peanut-list a").click(function(){
        var index = $(this).index();
        $('html,body').animate({
            scrollTop: (index) * h + phone - 156}, 600
        );
    });
});

function setbgHeight(x) {
    if ($(window).width() > 1600) {
        cal = 1000;
        h = 820;
    }
    else if ($(window).width() > 1025) {
        cal = 750;
        h = 620;
    }
    else if ($(window).width() > 767) {
        cal = 750;
        h = 500;
    }
    else {
        cal = 750;
        h = 500;
    }
}
function scrollFix() {
    // console.log(cal);
    bgHeight = $(".features-content-section").height() + $(".features-content-section").offset().top - cal;    
    // console.log(window.pageYOffset);
    if (window.pageYOffset > peanut - 408 && window.pageYOffset < bgHeight) {
        $(".peanut-list").addClass("sticky");
        $(".peanut-list").removeClass("stickyBot");
    }
    else if (window.pageYOffset > bgHeight) {
        $(".peanut-list").removeClass("sticky");
        $(".peanut-list").addClass("stickyBot");
    } 
    else {
        $(".peanut-list").removeClass("sticky");
        $(".peanut-list").removeClass("stickyBot");    
    }

    if (window.pageYOffset > phone - 156 && window.pageYOffset < bgHeight) {
        $(".feature-items").children().addClass("stickyy");
        $(".feature-items").children().removeClass("stickyyBot");
    } 
    else if (window.pageYOffset > bgHeight) {
        $(".feature-items").children().removeClass("stickyy");
        $(".feature-items").children().addClass("stickyyBot");
    }
    else {
        $(".feature-items").children().removeClass("stickyy");
        $(".feature-items").children().removeClass("stickyyBot");
    }
    
    var n = Math.floor((window.pageYOffset - (phone - 156))/h) + 1;
    if (n > 6) {n = 6}
    else if (n < 1) {n = 1}
    $(".feature-items .content:not(#f-"+n+")").fadeOut(100);
    $(".feature-items #f-"+n).fadeIn(100);
    console.log(n);
}

function changeScrollClass() {
    var n = Math.floor((window.pageYOffset - (phone - 156))/h) + 1;
    if (n > 6) {n = 6}
    else if (n < 1) {n = 1}
    $(".peanut-list a.active").removeClass("active");
    $(".peanut-list").children().eq(n-1).addClass("active");
}