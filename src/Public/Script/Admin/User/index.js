$("document").ready(function () {
    $(".table").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
        }
    });

    $('.js-single-select').select2();

    $('.js-multiple-select-with-tags').select2({
        tags: true
    });

    $(".generateRandomPassword").click(function() {
        console.log("Duy");
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        $(this).parent().parent().find("input").val(text);
    });

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });

    $('input[type="checkbox"].minimal').on('ifToggled', function(event) {
        var username = $(this).data('username');
        window.location.href = `/quan-tri/tai-khoan/doi-trang-thai/${username}`;
    });
});