$("document").ready(function () {
    $(".table").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
        }
    });

    CKEDITOR.replace('homeText');
    CKEDITOR.replace('featureText');

    const featuresList = JSON.parse($('input[name=featuresList]').val());

    for (const feature of featuresList) {
        CKEDITOR.replace($(`#featureEditModal-${feature.id} textarea[name=homeText]`)[0]);
        CKEDITOR.replace($(`#featureEditModal-${feature.id} textarea[name=featureText]`)[0]);

    }
});