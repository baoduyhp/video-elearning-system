$(document).ready(function() {
    $('.js-single-select').select2();

    $('select[name=link]').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.id === "custom")
            $('input[name=customLink]').removeAttr('disabled');
        else $('input[name=customLink]').attr('disabled', 'disabled');
    });

    $(".table").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
        }
    });
});


