CKEDITOR.replace('content', {
    "extraPlugins": 'imagebrowser',
    "imageBrowser_listUrl": "/files"
});

$('.js-single-select').select2();

$('.js-multiple-select-with-tags').select2({
    tags: true
});