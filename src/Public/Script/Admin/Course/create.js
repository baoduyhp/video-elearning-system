CKEDITOR.replace('extraInfo', {
    "extraPlugins": 'imagebrowser',
    "imageBrowser_listUrl": "/files"
});

$('.js-single-select').select2();

$('.js-multiple-select-with-tags').select2({
    tags: true
});

$(document).ready(function() {
    Inputmask("integer", {
        groupSeparator: ',',
        inputType: 'text',
        rightAlign: false,
        autoGroup: true
    }).mask($('input[name=fee]'));

    Inputmask("integer", {
        groupSeparator: ',',
        inputType: 'text',
        rightAlign: false,
        autoGroup: true
    }).mask($('input[name=maxStudent]'));
    
});