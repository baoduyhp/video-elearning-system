$("document").ready(function () {
    $(".table").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
        }
    });

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });

    $('input[type="checkbox"].minimal').on('ifToggled', function(event) {
        var courseId = $(this).data('course-id');
        window.location.href = `/quan-tri/khoa-hoc/doi-trang-thai/${courseId}`;
    });
});