$(document).ready(function() {
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });

    $('input[type="checkbox"].minimal').on('ifToggled', function(event) {
        var featureCode = $(this).data('feature-code');
        var parentCode = $(this).data('parent-code');
        if (parentCode)
            $(`input[data-feature-code=${parentCode}].minimal`).iCheck('check');
    });
})