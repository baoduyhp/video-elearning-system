var socket = io(`http://${host}/messages`);
var registeration = JSON.parse($("input[name=registeration]").val());

moment.locale('vi');

socket.on('messageFromClientSuccessfully', function(message) {
    if (message.fromClient.sid === registeration.sid) {
        var htmlString =
        `
        <div class="direct-chat-msg">
            <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">${registeration.fullName}</span>
                <span class="direct-chat-timestamp pull-right">${moment(message.createdAt).format("DD/MM/YYYY HH:mm:ss")}</span>
            </div>
            <img class="direct-chat-img" src="http://via.placeholder.com/128x128" alt="Message User Image">
            <div class="direct-chat-text">
            ${message.content}
            </div>
        </div>
        `;
        $("#messageBox").append(htmlString);
    }
});

socket.on('messageFromAdminSuccessfully', function(message) {
    if (message.toClient.sid === registeration.sid) {
        var htmlString =
        `
        <div class="direct-chat-msg right">
            <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">${message.fromAdmin.displayName}</span>
                <span class="direct-chat-timestamp pull-right">${moment(message.createdAt).format("DD/MM/YYYY HH:mm:ss")}</span>
            </div>
            <img class="direct-chat-img" src="http://via.placeholder.com/128x128" alt="Message User Image">
            <div class="direct-chat-text">
            ${message.content}
            </div>
        </div>
        `;
        $("#messageBox").append(htmlString);
    }

    $("input[name=content]").val('');
    $("input[name=content]").focus();
});

$("#chatForm").submit(function(event) {
    event.preventDefault();
    const content = $(this).serializeArray().find(c => c.name === 'content').value;
    const username = $(this).serializeArray().find(c => c.name === 'username').value;    
    socket.emit('messageFromAdmin', {
        content: content,
        username: username,
        sid: registeration.sid
    });
});