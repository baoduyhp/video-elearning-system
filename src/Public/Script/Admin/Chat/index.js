var socket = io(`http://${host}/messages`);

$("document").ready(function () {
    var table = $(".table").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }, {
                "targets": [ 4 ],
                "type": "html",
                "className": "text-center"
            }
        ],
        "aaSorting": []
    });

    socket.on('initChat', function(data) {
        
        const tableLength = table.rows().data().length;
        for (let i = 0; i < tableLength; i++) {
            var rowData = table.row(i).data();
            if (rowData[0] === data.sid) {
                table.row(i).remove().draw();
                break;
            }
        }

        table.destroy();

        const htmlRow = 
        `
        <tr>
            <td class='text-center'>${data.sid}</td>
            <td class='text-left'>${data.fullName}</td>
            <td class='text-center'>${data.phoneNumber}</td>
            <td class='text-center'>${data.email}</td>
            <td class='text-center'><a href='/chat/${data.sid}' target='_blank'>Vào chat</a></td>
        </tr>
        `;

        $('table tbody').prepend(htmlRow);


        table = $(".table").DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json"
            },
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false
                }, {
                    "targets": [ 4 ],
                    "type": "html",
                    "className": "text-center"
                }
            ],
            "aaSorting": []
        });
        
    });
});