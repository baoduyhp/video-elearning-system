/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.language = "vi";
    config.removePlugins = 'save';
    config.removePlugins = 'elementspath';

    config.filebrowserBrowseUrl = "/public/components/ckfinder/ckfinder.html";
    config.filebrowserImageUrl = "/public/components/ckfinder/ckfinder.html?type=Images";
    config.filebrowserFlashUrl = "/public/components/ckfinder/ckfinder.html?type=Flash";
    config.filebrowserUploadUrl = "/public/components/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files";
    config.filebrowserImageUploadUrl = "/public/components/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images";
    config.filebrowserFlashUploadUrl = "/public/components/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash";
};
