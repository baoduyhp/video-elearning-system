@import "../Shared/Partials/variables";

.search-container {
    display: flex;
    justify-content: space-between;
    .search-result {
        width: 100%;
        @media (min-width: $screen-md) {
            width: 66%;
        }
        .r-text {
            span {
                padding-left: 5px;
            }
        }
        .r-count {
            padding-top: 10px;
        }
        .r-list {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            padding-top: 45px;
            .r-item {
                padding-bottom: 60px;
                width: 100%;
                display: flex;
                justify-content: space-between;
                @media (min-width: $screen-sm) {
                    width: 31.9%;
                    display: block;
                }
                @media (min-width: $screen-md) {
                    width: 48%;
                }
                .r-img {
                    height: 150px;
                    width: 55%;
                    a {
                        display: inline;
                    }
                    @media (min-width: $screen-sm) {
                        height: 189px;
                        width: auto;
                    }
                    @media (min-width: $screen-lg) {
                        height: 253px;
                    }
                    img {
                        height: 100%;
                        object-fit: cover;
                        width: 100%;
                    }
                }
                .r-title {
                    padding-top: 5px;
                    width: 40%;
                    @media (min-width: $screen-sm) {
                        padding-top: 24px;
                        width: auto;                        
                    }
                }
                .r-about {
                    line-height: 28px;
                    padding-top: 10px;
                    display: none;
                    @media (min-width: $screen-sm) {
                        display: block;
                    }
                }
            }
        }
        .r-paginate {
            display: flex;
            justify-content: center;
            align-content: center;
            ul {
                list-style: none;
                padding: 0 25px;
                @media (min-width: $screen-sm) {
                    padding: 0 50px;
                }
                li {
                    display: inline;
                    color: $color-7E;
                    font-family: $font-SF-Light;
                    font-size: $font-16;
                    line-height: 30px;
                    padding: 3px 8px;
                    @media (min-width: $screen-sm) {
                        padding: 10px 15px;
                    }
                    @media (min-width: $screen-lg) {
                        padding: 10px 17px;    
                    }
                    &.active {
                        background-image: linear-gradient(45deg, #AB47BC 0%, #8E24AA 36%, #5D2E86 100%);
                        border-radius: 100px;
                        color: $color-white;

                    }
                    &:not(:last-child) {
                        margin-right: 30px;
                        @media (min-width: $screen-md) {
                            margin-right: 50px;
                        }
                    }
                }
            }
            .icons {
                display: block;
                padding-top: 8px;
            }
        }
        .related-post-section {
            border-top: 1px solid $color-99;
            padding-top: 45px;
            margin-top: 90px;
        }
    
    }
    .feature-post {
        display: none;
        @media (min-width: $screen-md) {
            display: block;
            width: 28%;
        }
        .f-title {
            padding-top: 20px;
        }
        .f-list {
            padding-top: 40px;
            .f-post {
                align-items: center;
                display: flex;
                justify-content: space-between;
                padding-bottom: 28px;
                .f-p-img {
                    width: 40%;
                    height: 93.5px;
                    a {
                        display: inline;
                    }
                    img {
                        height: 100%;
                        object-fit: cover;
                        width: 100%;
                    }
                }
                .f-p-title {
                    color: $color-48;
                    font-family: $font-SF-Medium;
                    line-height: 28px;
                    width: 55%;
                }
            }
        }
    }
}