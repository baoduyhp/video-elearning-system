import { File } from "Models/file.entity";

export interface FileService {
    
    add(file: File): Promise<void>;

    update(file: File): Promise<void>;

    delete(id: number): Promise<void>;

    findAll(): Promise<File[]>;

    findOne(id: number): Promise<File>;
}