import { AuthenticationMessageDTO } from "Helpers/DTO/authentication.dto";
import { User } from "Models/user.entity";

'use strict';

export interface JwtOptions {
    algorithm: string;
    expiresIn: number | string;
    jwtid: string;
}

export interface AuthenticationService {
    sign(credentials: { username: string, password: string}): Promise<AuthenticationMessageDTO>;
    verify(token: string): Promise<User>;
}

