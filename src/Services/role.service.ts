import { Role } from "Models/role.entity";

export interface RoleService {
    addOrUpdate(feature: Role): Promise<void>;

    findAll(): Promise<Role[]>;

    findOneByTitle(title: string): Promise<Role>;
    
    findOne(id: number): Promise<Role>;

    delete(id: number): Promise<void>;
}