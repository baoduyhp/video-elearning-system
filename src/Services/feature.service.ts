import { Feature } from "Models/feature.entity";

export interface FeatureService {
    addOrUpdate(feature: Feature): Promise<void>;

    findAll(): Promise<Feature[]>;

    findOne(code: string): Promise<Feature>;
    
    deleteAll(): Promise<void>;
}