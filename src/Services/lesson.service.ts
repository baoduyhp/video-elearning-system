import { Lesson } from "Models/lesson.entity";

export interface LessonService {

    add(lesson: Lesson): Promise<void>;

    update(lesson: Lesson): Promise<void>;

    delete(id: number): Promise<void>;

    publish(id: number): Promise<void>;

    findAllByCourseId(courseId: number): Promise<Lesson[]>;

    findOne(id: number): Promise<Lesson>;
}