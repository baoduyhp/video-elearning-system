import { User } from '../Models/user.entity';

export interface UserService {

    findAll(): Promise<User[]>;

    findOne(username: string): Promise<User>;

    findOneById(id: number): Promise<User>;

    add(user: User): Promise<User>;

    save(user: User): Promise<void>;
}