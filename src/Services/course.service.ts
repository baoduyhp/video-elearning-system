import { Course } from "Models/course.entity";

export interface CourseService {

    add(course: Course): Promise<void>;

    update(course: Course): Promise<void>;

    delete(id: number): Promise<void>;

    publish(id: number): Promise<void>;

    findAll(): Promise<Course[]>;

    findOne(id: number): Promise<Course>;
}