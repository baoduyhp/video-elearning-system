import { Topic } from "Models/topic.entity";

export interface TopicService {
    
    add(topic: Topic): Promise<void>;

    update(topic: Topic): Promise<void>;

    delete(id: number): Promise<void>;

    findAll(): Promise<Topic[]>;

    findOne(id: number): Promise<Topic>;
}