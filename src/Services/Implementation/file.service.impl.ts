import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as fs from 'fs';

import { File } from 'Models/file.entity';
import { FileService } from 'Services/file.service';

@Injectable()
export class FileServiceImpl implements FileService {
    private readonly repository: Repository<File>;

    constructor(@InjectRepository(File) repository: Repository<File>) {
        this.repository = repository;
    }

    async add(file: File): Promise<void> {
        var fileToInsert: File = new File();
        fileToInsert.filename = file.filename;
        fileToInsert.filetype = file.filetype;
        fileToInsert.displayName = file.displayName;
        fileToInsert.size = file.size;
        fileToInsert.path = file.path;
        fileToInsert.createdUser = file.createdUser;
        
        await this.repository.insert(fileToInsert);
    }

    async update(file: File): Promise<void> {
        await this.repository.save(file);
        
    }

    async delete(id: number): Promise<void> {
        var fileToDelete: File = await this.repository.findOne(id);
        fileToDelete.hidden = true;

        await this.repository.save(fileToDelete);
    }

    async findOne(id: number): Promise<File> {
        return await this.repository.findOne(id, {
            relations: [ "createdUser" ]
        });
    }

    async findAll(): Promise<File[]> {
        return await this.repository.find({
            order: {
                updatedAt: 'DESC'
            },
            relations: ["createdUser"]
        });
    }
}