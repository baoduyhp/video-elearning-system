'use strict';

import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import { Injectable, Dependencies } from '@nestjs/common';
import { AuthenticationService, JwtOptions } from '../authentication.service';
import { UserService } from '../user.service';
import { UserServiceImpl } from './user.service.impl';
import { User } from '../../Models/user.entity';
import { AuthenticationMessageDTO } from 'Helpers/DTO/authentication.dto';
import { AuthenticationStatus } from 'Helpers/Constant/authentication.constant';
import { UserStatus } from 'Helpers/Constant/user.constant';

@Injectable()
@Dependencies(UserServiceImpl)
export class AuthenticationServiceImpl implements AuthenticationService {

    private userService: UserService;

    constructor(userService: UserService) {
        this.userService = userService;
    }

    private _options: JwtOptions = {
        algorithm: 'HS256',
        expiresIn: '7 days',
        jwtid: process.env.JWT_ID || 'F6Iyom5DHCKa8QK2SOlKkFXoFO85d4Jr'
    }

    get options(): JwtOptions {
        return this._options;
    }

    set options(value: JwtOptions) {
        this._options.algorithm = value.algorithm;
    }

    public async sign(credentials: { username: string, password: string}): Promise<AuthenticationMessageDTO> {
        const user: User = await this.userService.findOne(credentials.username);
        var message: AuthenticationMessageDTO = { 
            status: null,
            token: null
        };

        if (!user || user.status === UserStatus.INACTIVE) {
            message.status = AuthenticationStatus.USER_NOT_FOUND;
        }
        else if (user.password !== crypto.createHmac('sha256', credentials.password).digest('hex')) {
            message.status = AuthenticationStatus.PASSWORD_NOT_MATCH;
        }
        else {
            message.status = AuthenticationStatus.OK;
            message.token = await jwt.sign({
                username: user.username
            }, process.env.SECRET_KEY, this._options);
        }
        return message;
    }

    public async verify(token: string): Promise<User> {
        try {
            var decoded: User = await jwt.verify(token, process.env.SECRET_KEY, this._options);
            return decoded;
        }
        catch(err) {
            return null;
        }
    }
}