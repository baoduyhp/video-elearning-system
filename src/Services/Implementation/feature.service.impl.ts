import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Code } from 'typeorm';

import { Feature } from '../../Models/feature.entity';
import { FeatureService } from '../feature.service';

@Injectable()
export class FeatureServiceImpl implements FeatureService {
    private readonly repository: Repository<Feature>;

    constructor(@InjectRepository(Feature) repository: Repository<Feature>) {
        this.repository = repository;
    }

    async addOrUpdate(feature: Feature): Promise<void> {
        const existedFunc = await this.repository.findOne({ code: feature.code });
        if (!existedFunc)
            await this.repository.insert(feature);
        else {
            existedFunc.name = feature.name;
            existedFunc.menuTitle = feature.menuTitle;
            existedFunc.link = feature.link;
            existedFunc.onMenu = feature.onMenu;
            existedFunc.icon = feature.icon;
            existedFunc.alwaysVisible = feature.alwaysVisible;
            existedFunc.order = feature.order;
            existedFunc.parentFeature = feature.parentFeature;

            await this.repository.save(existedFunc);
        }
    }

    async findAll(): Promise<Feature[]> {
        return await this.repository.find({ 
            order: {
                order: "ASC"
            },
            relations: [ "parentFeature" ]
        });
    }

    async findOne(code: string): Promise<Feature> {
        return await this.repository.findOne({
            code: code
        }, {
            relations: [ "parentFeature" ]
        });
    }

    async deleteAll(): Promise<void> {
        const functions: Feature[] = await this.repository.find();
        for (const func of functions) {
            await this.repository.delete(func.code);
        }
    }
}