import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from '../../Models/user.entity';
import { UserService } from '../user.service';

@Injectable()
export class UserServiceImpl implements UserService {
    private readonly repository: Repository<User>;

    constructor(@InjectRepository(User) repository: Repository<User>) {
        this.repository = repository;
    }

    async findAll(): Promise<User[]> {
        return await this.repository.find({
            relations: ["role", "role.features"]
        });
    }

    async findOne(username: string): Promise<User> {
        return await this.repository.findOne({ username }, {
            relations: ["role", "role.features"]
        });
    }

    async findOneById(id: number): Promise<User> {
        return await this.repository.findOne(id, {
            relations: ["role", "role.features"]
        });
    }

    async add(user: User): Promise<User> {
        const existedUser = await this.findOne(user.username);
        
        if (existedUser)
            return Promise.reject("EXISTED_USER");
        
        this.repository.insert(user);
    }

    async save(user: User): Promise<void> {
        await this.repository.save(user);
    }
}