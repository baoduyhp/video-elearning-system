import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as fs from 'fs';

import { Topic } from 'Models/topic.entity';
import { TopicService } from 'Services/topic.service';

@Injectable()
export class TopicServiceImpl implements TopicService {
    private readonly repository: Repository<Topic>;

    constructor(@InjectRepository(Topic) repository: Repository<Topic>) {
        this.repository = repository;
    }

    async add(topic: Topic): Promise<void> {
        await this.repository.save(topic);
    }

    async update(topic: Topic): Promise<void> {
        let topicToSave: Topic = await this.repository.findOne(topic.id);
        topicToSave.name = topic.name.toString();

        await this.repository.save(topicToSave);
        
    }

    async delete(id: number): Promise<void> {
        await this.repository.delete(id);
    }

    async findOne(id: number): Promise<Topic> {
        return await this.repository.findOne(id, {
            relations: [ "courses" ]
        });
    }

    async findAll(): Promise<Topic[]> {
        return await this.repository.find({
            order: {
                updatedAt: 'DESC'
            },
            relations: ["courses"]
        });
    }
}