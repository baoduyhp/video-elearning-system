import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as fs from 'fs';

import { Course } from 'Models/course.entity';
import { CourseService } from 'Services/course.service';

@Injectable()
export class CourseServiceImpl implements CourseService {
    private readonly repository: Repository<Course>;

    constructor(@InjectRepository(Course) repository: Repository<Course>) {
        this.repository = repository;
    }

    async add(course: Course): Promise<void> {
        await this.repository.save(course);
    }

    async update(course: Course): Promise<void> {
        let courseToSave: Course = await this.repository.findOne(course.id);
        courseToSave.name = course.name.toString();
        courseToSave.maxStudent = course.maxStudent;
        courseToSave.fee = course.fee;
        courseToSave.extraInfo = course.extraInfo;
        courseToSave.topic = course.topic;
        courseToSave.published = course.published;

        await this.repository.save(courseToSave);
        
    }

    async delete(id: number): Promise<void> {
        let courseToSave: Course = await this.repository.findOne(id);
        courseToSave.hidden = true;

        await this.repository.save(courseToSave);
    }

    async publish(id: number): Promise<void> {
        let courseToSave: Course = await this.repository.findOne(id);
        courseToSave.published = !courseToSave.published;

        await this.repository.save(courseToSave);
    }

    async findOne(id: number): Promise<Course> {
        return await this.repository.findOne(id, {
            where: {
                hidden: false
            },
            relations: [ "topic", "lessons" ]
        });
    }

    async findAll(): Promise<Course[]> {
        return await this.repository.find({
            where: {
                hidden: false
            },
            order: {
                updatedAt: 'DESC'
            },
            relations: ["topic", "lessons"]
        });
    }
}