import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Code, RelationCount } from 'typeorm';

import { RoleService } from '../role.service';
import { Role } from 'Models/role.entity';

@Injectable()
export class RoleServiceImpl implements RoleService {
    private readonly repository: Repository<Role>;

    constructor(@InjectRepository(Role) repository: Repository<Role>) {
        this.repository = repository;
    }

    async addOrUpdate(role: Role): Promise<void> {
        await this.repository.save(role);
    }

    async findAll(): Promise<Role[]> {
        return await this.repository.find({ 
            order: {
                updatedAt: "DESC"
            },
            relations: [ "features" ]
        });
    }

    async findOneByTitle(title: string): Promise<Role> {
        return await this.repository.findOne(
            {
                title: title
            }, 
            {
                relations: [ "features" ]
            }
        );
    }

    async findOne(id: number): Promise<Role> {
        return await this.repository.findOne(id,
            {
                relations: [ "features", "users" ]
            }
        );
    }

    async delete(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}