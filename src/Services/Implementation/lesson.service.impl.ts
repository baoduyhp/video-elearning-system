import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Lesson } from 'Models/lesson.entity';
import { LessonService } from 'Services/lesson.service';

@Injectable()
export class LessonServiceImpl implements LessonService {
    private readonly repository: Repository<Lesson>;

    constructor(@InjectRepository(Lesson) repository: Repository<Lesson>) {
        this.repository = repository;
    }

    async add(lesson: Lesson): Promise<void> {
        await this.repository.save(lesson);
    }

    async update(lesson: Lesson): Promise<void> {
        let lessonToSave: Lesson = await this.repository.findOne(lesson.id);
        lessonToSave.name = lesson.name.toString();
        lessonToSave.content = lesson.content;
        lessonToSave.videoUrl = lesson.videoUrl;
        lessonToSave.published = false;
        lessonToSave.files = lesson.files;

        await this.repository.save(lessonToSave);
        
    }

    async delete(id: number): Promise<void> {
        let lessonToSave: Lesson = await this.repository.findOne(id);
        lessonToSave.hidden = true;

        await this.repository.save(lessonToSave);
    }

    async publish(id: number): Promise<void> {
        let lessonToSave: Lesson = await this.repository.findOne(id);
        lessonToSave.published = !lessonToSave.published;

        await this.repository.save(lessonToSave);
    }

    async findOne(id: number): Promise<Lesson> {
        return await this.repository.findOne(id, {
            where: {
                hidden: false
            },
            relations: [ "files", "course" ]
        });
    }

    async findAllByCourseId(courseId: number): Promise<Lesson[]> {
        return await this.repository.find({
            where: {
                hidden: false,
                course: {
                    id: courseId
                }
            },
            order: {
                order: 'ASC'
            },
            relations: [ "files", "course" ]
        })
    }
}