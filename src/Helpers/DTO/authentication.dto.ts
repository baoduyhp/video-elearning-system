export interface AuthenticationDTO {
    readonly username: string;
    readonly password: string;
}
export interface AuthenticationMessageDTO {
    status: number;
    token: string;
}