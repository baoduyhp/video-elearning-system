import * as jwt from 'jsonwebtoken';
import { Injectable, CanActivate, ExecutionContext, UnauthorizedException, Dependencies } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request, Response } from 'express';
import { AuthenticationServiceImpl } from 'Services/Implementation/authentication.service.impl';
import { User } from 'Models/user.entity';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private readonly reflector: Reflector,
    private authenticationService: AuthenticationServiceImpl) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
      let roles = this.reflector.get<string[]>('roles', context.getHandler()); roles = roles ? roles : [];
      const request: Request = context.switchToHttp().getRequest();

      var user: User = request.session.user;
      var featureCode: string[] = user.role.features.map(x => x.code);
      if (roles.filter(value => -1 !== featureCode.indexOf(value)).length >= 0) return true;

      throw new UnauthorizedException();
    }
}