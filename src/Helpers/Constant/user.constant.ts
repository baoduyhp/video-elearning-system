export class UserStatus {
    public static INACTIVE: string = "INACTIVE";

    public static ACTIVE: string = "ACTIVE";
}