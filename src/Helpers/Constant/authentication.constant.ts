export class AuthenticationStatus {
    public static USER_NOT_FOUND: number = 0;

    public static PASSWORD_NOT_MATCH: number = 1;

    public static OK = 2;
}