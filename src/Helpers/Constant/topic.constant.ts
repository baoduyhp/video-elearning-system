export class TopicMessage {
    public static ADD_TOPIC_SUCCESSFULLY: string = "Thêm chủ đề mới thành công";

    public static EXISTED_REFERENCE: string = "Còn tồn tại khóa học thuộc chủ đề này";
}