import { Injectable, NestMiddleware, MiddlewareFunction, Dependencies } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as moment from 'moment';
import * as truncatise from 'truncatise';
import * as numeral from 'numeral';
import * as randomstring from 'randomstring';

@Injectable()
export class LocalMiddleware implements NestMiddleware {
    resolve(...args: any[]): MiddlewareFunction {
        return async (req: Request, res: Response, next: NextFunction) => {
            moment.locale('vi');
            req.app.locals.moment = moment;
            req.app.locals.truncatise = truncatise;
            req.app.locals.numeral = numeral;
            req.app.locals.randomstring = randomstring;
            next();
        };
    }
}