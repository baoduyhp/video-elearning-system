import { Injectable, NestMiddleware, MiddlewareFunction, Dependencies, UnauthorizedException } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { FeatureService } from 'Services/feature.service';
import { FeatureServiceImpl } from 'Services/Implementation/feature.service.impl';
import { Feature } from 'Models/feature.entity';
import { AuthenticationServiceImpl } from 'Services/Implementation/authentication.service.impl';
import { AuthenticationService } from 'Services/authentication.service';
import { User } from 'Models/user.entity';
import { UserServiceImpl } from '../../Services/Implementation/user.service.impl';
import { UserService } from 'Services/user.service';

@Injectable()
@Dependencies(FeatureServiceImpl, AuthenticationServiceImpl, UserServiceImpl)
export class MenuMiddleware implements NestMiddleware {

    constructor(private featureService: FeatureService,
        private authenticationService: AuthenticationService,
        private userService: UserService) {
    }

    resolve(...args: any[]): MiddlewareFunction {
        return async (req: Request, res: Response, next: NextFunction) => {
            if (req.baseUrl === '/quan-tri/dang-nhap' || req.baseUrl === '/quan-tri/khoi-tao-he-thong')
                return next();

            if (req.method.toUpperCase() === 'GET') {
                var token = req.cookies['ves'];
                if (!token) throw new UnauthorizedException();

                var decoded = await this.authenticationService.verify(token);
                if (!decoded) throw new UnauthorizedException();

                var username: string = decoded["username"];
                var user: User = await this.userService.findOne(username);
                req.session.user = user;

                var features: Feature[] = user.role.features.filter(feature => feature.onMenu).sort((a, b) => a.order - b.order);
                var indexOfCurrentFeature: number = features.findIndex(value => value.link === req.baseUrl);
                if (indexOfCurrentFeature >= 0)
                    features[indexOfCurrentFeature].active = true;
                req.app.locals.features = features;
                req.app.locals.user = user;
                req.app.locals.host = process.env.DOMAIN;
            }
            next();
        };
    }
}