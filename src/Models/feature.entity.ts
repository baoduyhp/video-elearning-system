import { Entity, Column, PrimaryColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, OneToMany, PrimaryGeneratedColumn, ManyToOne, JoinTable } from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class Feature {
    @PrimaryColumn("varchar", { length: 24 })
    code: string;

    @Column("text")
    name: string;

    @Column()
    onMenu: boolean;

    @Column()
    menuTitle: string;

    @Column()
    link: string;

    @Column()
    icon: string;

    @Column()
    alwaysVisible: boolean

    @Column()
    order: number;

    @Column({ default: false })
    active: boolean;

    @ManyToOne(type => Feature, parentFeature => parentFeature.childrenFeatures)
    parentFeature: Feature;

    @OneToMany(type => Feature, feature => feature.parentFeature)
    childrenFeatures: Feature[];

    @ManyToMany(type => Role, role => role.features, {
        cascade: ["insert", "update"]
    })
    roles: Role[];
}