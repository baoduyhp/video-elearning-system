import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { Course } from './course.entity';
import { Mqc } from './mqc.entity';


@Entity()
export class Topic {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255 })
    name: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(type => Course, course => course.topic)
    courses: Course[];

    @ManyToMany(type => Mqc, mqc => mqc.topics)
    @JoinTable()
    mqcs: Mqc[];
}