import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToMany, JoinTable, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Course } from './course.entity';

@Entity()
export class Participation {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.participations)
    user: User;

    @ManyToOne(type => Course, course => course.participations)
    course: Course;

    @CreateDateColumn()
    createdAt: Date;
    

}