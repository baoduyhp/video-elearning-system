import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinTable, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Course } from './course.entity';
import { File } from './file.entity';

@Entity()
export class Lesson {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255 })
    name: string;

    @Column("varchar")
    slug: string;

    @Column({ nullable: true })
    videoUrl: string;

    @Column("text")
    content: string;

    @Column("boolean", { default: false })
    published: boolean;

    @Column("boolean", { default: false })
    hidden: boolean;

    @Column("integer")
    order: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToMany(type => File, file => file.lessons, {
        cascade: ["insert", "update"]
    })
    @JoinTable()
    files: File[];

    @ManyToOne(type => Course, course => course.lessons)
    course: Course;

}