import { Question } from './question.entity';
import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity()
export class Choice {
    @PrimaryGeneratedColumn()
    id: number;


    @Column("text")
    value: string;

    @Column("boolean")
    is_ans: boolean;

    @ManyToOne(type => Question, question => question.choices)
    question: Question;

}