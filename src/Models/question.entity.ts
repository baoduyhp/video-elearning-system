import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { Choice } from './choice.entity';
import { Mqc } from './mqc.entity';

@Entity()
export class Question {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255 })
    name: string;

    @Column("text")
    content: string;

    @OneToMany(type => Choice, choice => choice.question)
    choices: Choice[];

    @ManyToOne(type => Mqc, mqc => mqc.questions)
    mqc: Mqc;
}