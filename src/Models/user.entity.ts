import { Entity, Column, PrimaryColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, OneToMany, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Role } from './role.entity';
import { Course } from './course.entity';
import { File } from './file.entity';
import { Participation } from './participant.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    
    @PrimaryColumn("varchar", { length: 255 })
    username: string;

    @Column({ nullable: true })
    ggid: string;

    @Column("varchar", { length: 128 })
    password: string;

    @Column("varchar", { length: 255 })
    displayName: string;

    @Column("boolean", { default: true })
    gender: boolean;

    @Column("boolean", { default: true })
    isAdmin: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    status: string;

    @ManyToOne(type => Role, role => role.users)
    role: Role;

    @OneToMany(type => File, file => file.createdUser)
    files: File[];

    @OneToMany(type => Participation, participation => participation.user)
    participations: Participation[];
}