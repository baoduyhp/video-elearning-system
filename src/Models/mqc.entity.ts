import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Topic } from './topic.entity';
import { Course } from './course.entity';
import { Question } from './question.entity';

@Entity()
export class Mqc {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255 })
    name: string;

    @Column("text")
    content: string;

    @ManyToMany(type => Topic, topic => topic.mqcs)
    topics: Topic[];

    @ManyToMany(type => Course, course => course.mqcs)
    courses: Course[];

    @OneToMany(type => Question, question => question.mqc)
    @JoinTable()
    questions: Question[];
}