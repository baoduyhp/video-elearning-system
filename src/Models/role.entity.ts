import { User } from 'Models/user.entity';
import { Feature } from './feature.entity';
import { Entity, Column, PrimaryColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, OneToMany, ManyToOne, JoinTable, PrimaryGeneratedColumn } from 'typeorm';


@Entity()
export class Role {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255 })
    title: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(type => User, user => user.role)
    users: User[];

    @ManyToMany(type => Feature, feature => feature.roles, {
        cascade: ["insert", "update"]
    })
    @JoinTable()
    features: Feature[];
}