import { Lesson } from './lesson.entity';
import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToMany, JoinTable, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class File {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar")
    filename: string;

    @Column("varchar")
    filetype: string;

    @Column("varchar")
    displayName: string;

    @Column()
    size: number;

    @Column("varchar")
    path: string;

    @Column("boolean", { default: false })
    hidden: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.files)
    createdUser: User

    @ManyToMany(type => Lesson, lesson => lesson.files, {
        cascade: ["insert", "update"]
    })
    lessons: Lesson[];

}