import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany, CreateDateColumn, UpdateDateColumn, JoinTable } from 'typeorm';
import { Lesson } from './lesson.entity';
import { Topic } from './topic.entity';
import { Mqc } from './mqc.entity';
import { User } from './user.entity';
import { Participation } from './participant.entity';

@Entity()
export class Course {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar")
    name: string;

    @Column("varchar")
    slug: string;

    @Column("integer", { nullable: true })
    maxStudent: number;

    @Column("integer", { nullable: true })
    fee: number;

    @Column("boolean", { default: false })
    published: boolean;

    @Column("boolean", { default: false })
    hidden: boolean;

    @Column("text", { nullable: true })
    extraInfo: string;

    @ManyToOne(type => Topic, topic => topic.courses)
    topic: Topic;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(type => Lesson, lesson => lesson.course)
    lessons: Lesson[];

    @ManyToMany(type => Mqc, mqc => mqc.courses)
    @JoinTable()
    mqcs: Mqc[];

    @OneToMany(type => Participation, participation => participation.course)
    participations: Participation[];
}