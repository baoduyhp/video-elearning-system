import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { MainModule } from './Modules/Main/main.module';
import { AdminModule } from 'Modules/Admin/admin.module';
import { VideoModule } from './Modules/Video/video.module';



@Module({
  imports: [ MainModule, AdminModule, VideoModule ]
})
export class AppModule { }
